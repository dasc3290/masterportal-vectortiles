> Die [englische Version](/README.md) aufrufen.

# Masterportal

Das Masterportal ist ein Baukasten für Geo-Anwendungen im Web auf Basis von [OpenLayers](https://openlayers.org), [Vue.js](https://vuejs.org/) und [Backbone.js](https://backbonejs.org). Das Masterportal ist OpenSource Software. Es ist unter der [MIT-Lizenz](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/License.txt) veröffentlicht.

Das Masterportal ist ein Projekt der [Geowerkstatt Hamburg](https://www.hamburg.de/geowerkstatt/).

## für Anwender

* [Download](https://bitbucket.org/geowerkstatt-hamburg/masterportal/downloads/)
* [Quickstart für Anwender](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/setup.de.md)
* [Remote Interface](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/remoteInterface.de.md)
* [Dokumentation für Anwender](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/doc.de.md)
* [Community Board (Anwender-Forum, Issue Tracker)](https://trello.com/c/qajdXkMa/110-willkommen)

## für Entwickler

* [Dokumentation für Entwickler](doc/devdoc.de.md)
* [Tutorial 01: Ein neues Modul erstellen (Scale Switcher)](https://bitbucket.org/geowerkstatt-hamburg/masterportal/src/dev/doc/vueTutorial.de.md)
* [Community Board (Entwicklerforum, Issue Tracker)](https://trello.com/c/qajdXkMa/110-willkommen)


[![BrowserStack Status](https://automate.browserstack.com/badge.svg?badge_key=aDNJT1VSRDlMVXNpRzJzYXQ4bHN0RERXbGpETmdQeDBMUlp0cEJkOWNPRT0tLVFJcndaSi9KWFBTM0FVWEZkYnhlS2c9PQ==--0ef330a6aef7023b1f50659a6d57f9369f988f53)](https://automate.browserstack.com/public-build/aDNJT1VSRDlMVXNpRzJzYXQ4bHN0RERXbGpETmdQeDBMUlp0cEJkOWNPRT0tLVFJcndaSi9KWFBTM0FVWEZkYnhlS2c9PQ==--0ef330a6aef7023b1f50659a6d57f9369f988f53)
